import React, { Component } from 'react';
import { Platform, ActivityIndicator, Button, View, Dimensions } from 'react-native';
import MapView, { Marker, Polyline, Polygon, PROVIDER_GOOGLE } from 'react-native-maps';
import Constants from 'expo-constants';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';

console.disableYellowBox = true;

export default class First extends Component {
  state = {
    location: null,
    locations: [],
    markers: [],
    errorMessage: null,
    auto: false,
    count: 0.001
  };

  componentDidMount() {
    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({
        errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
      });
    } else {
      this._getLocationAsync();
    }
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }

    Location.watchPositionAsync({
      accuracy : Location.Accuracy.BestForNavigation,
      timeInterval: 2000
    }, (location) => {
      const newLocation = {
        ...location.coords,
        // latitude: location.coords.latitude + this.state.count,
        // longitude: location.coords.longitude + this.state.count,
        latitudeDelta: 0.01,
        longitudeDelta: 0.01
      }

      this.setState({
        location: newLocation,
        count: this.state.count + 0.001
      });

      if(this.state.auto) {
        this.setState(prevState => {
          return {
            ...prevState,
            locations: [...prevState.locations, { latitude: location.coords.latitude, longitude: location.coords.latitude }],
            markers: [...prevState.markers, newLocation]
          }
        });
      }
    }).bind(this);
  };

  render() {
    const { location, locations, markers, auto } = this.state;

    if(!location) return <ActivityIndicator/>;

    return (
      <>
      <MapView
        style={{ flex: 1 }}
        provider={PROVIDER_GOOGLE}
        initialRegion={location}
      >
        {/* { markers.map((m, key) => {
          return (
            <Marker
              key={key}
              coordinate={m}
            />
          )
        })} */}

        {/* {locations.length ?
          <Polyline
            coordinates={locations}
            strokeColor="#000" // fallback for when `strokeColors` is not supported by the map-provider
            strokeWidth={6}
          /> : null} */}

          {locations.length > 3 ?
          <Polygon
            holes={[markers]}
            coordinates={markers}
            strokeColor="#F00"
            fillColor="rgba(255,0,0,0.5)"
            strokeWidth={1}
          /> : null}
        </MapView>
        <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
            }}>
          <Button
            title={ auto ? "Disable Automatic" : "Enable Automatic" }
            onPress={() => this.setState({ auto: !auto })}
          />

          <Button
            title="Clear"
            onPress={() =>
              this.setState({
                markers: [],
                auto: false,
                locations: []
              })
            }
          />
          <Button
            title="Add Marker"
            onPress={() =>
              this.setState(prev => {
                return {markers: [...prev.markers, location]}
              })
            }
          />
        </View>
      </>
    );
  }
}
