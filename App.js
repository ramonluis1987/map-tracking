import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import First from './components/first';
import Second from './components/second';

const TabNavigator = createBottomTabNavigator({
  First: First,
  Second: Second,
});

export default createAppContainer(TabNavigator);
